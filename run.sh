#!/bin/sh

#
# This script processes all Docker images and uploads them to the Gitlab registry if needed
#
export CI_REGISTRY=${CI_REGISTRY:-registry.gitlab.com}
export CI_PROJECT_PATH=${CI_PROJECT_PATH:-kimball.hewett/public}
export DOCKER_CLI_EXPERIMENTAL=enabled

IFS=$'\n'
for file in `find . -type f -name "Dockerfile.*"`
do
  {
    read firstLine
    read secondLine
  } < $file
  fromImage=${firstLine/"FROM "/} # The first line like `FROM docker:dind` with the image source name
  replaceToSubstr="\#TO "         # The second commented line like `#TO `
  toImageCandidate=${secondLine/$replaceToSubstr/}
  if [ "$toImageCandidate" != "" ] && [ "${#secondLine}" != "${#toImageCandidate}" ]; then
    toImage=$toImageCandidate

    toImage="${toImage#"${toImage%%[![:space:]]*}"}" # remove leading whitespaces
    toImage="${toImage%"${toImage##*[![:space:]]}"}" # remove trailing whitespaces

    echo "Checking $fromImage >>> GIVEN $toImage"
  else
    toImage=${CI_REGISTRY}/${CI_PROJECT_PATH}/$fromImage
    echo "Checking $fromImage >>> CALCULATED $toImage"
  fi

  docker manifest inspect $toImage > /dev/null
  if [ $? -eq 0 ]; then
    echo "-> [OK]"
  else
    dir="$(dirname "${file}")"
    echo "-> Image does not exist. Building..."
    docker build -t $toImage -f $file $dir
    echo "-> Built image. Pushing..."
    docker push $toImage
    echo "-> Image seems to be pushed."
  fi
done
